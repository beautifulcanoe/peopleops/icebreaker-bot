"""
Icebreaker bot!

Responds to a Slack slash command, usually /icebreaker.

Requires environment variables:

* PORT - for HTTP connections.
* SLASH_COMMAND - name of the slash command set in the Slack app.
* VERIFICATION_TOKEN - Slack verification token for this slash command.
"""

import logging
import os
import random
import sys

from flask import Flask
from flask import request
from flask import jsonify


APP = Flask(__name__)
COMMAND = os.environ.get('SLASH_COMMAND', 'icebreaker').strip()
DATA_FILE = 'icebreakers.txt'
HELP = 'Use /icebreaker to post a randomly chosen icebreaker to this channel.'
HOST = '0.0.0.0'
PORT = int(os.environ.get('PORT', 8080))
VERIFICATION_TOKEN = os.environ['VERIFICATION_TOKEN']


def load_icebreakers(filename):
    """Load icebreakers from a text file.

    Format is one icebreaker per line.
    """

    icebreakers = list()
    index = 1
    try:
        with open(filename, 'rt') as fpointer:
            line = fpointer.readline()
            while line:
                icebreakers.append(line.strip())
                index += 1
                line = fpointer.readline()
    except ValueError:
        logging.error('Could not parse %s on line %d.', filename, index)
        return None
    logging.debug('Loaded icebreakers from: %s', filename)
    return icebreakers


def get_icebreaker():
    """Return a random icebreaker from the data file."""

    text = None
    while text is None:
        text = random.choice(ICEBREAKERS)
    return text


def parse_slack_message(slack_message):
    """
    Consumes a slack POST message that was sent in JSON format.

    Returns a dict containing:
    {
        'username':<slack_username>,
        'command':<slash_command>,
        'text':<slash_command_arguments>,
        'channel_name':<slack_channel_command_issued_in>
    }
    Slack POST messages are formatted as JSON:
    {
        'token': 'uto4ItLoT82ceQoBpIvgtzzz',
        'team_id': 'T0C3TFAGL',
        'team_domain': 'my_team_name',
        'channel_id': 'D0C3VQDAS',
        'channel_name': 'directmessage',
        'user_id': 'U0C3TFAQ4',
        'user_name': 'my_username',
        'command': '/weather',
        'text': '2d6',
        'response_url': 'https://hooks.slack.com/...'
    }
    """

    if not VERIFICATION_TOKEN == slack_message['token']:
        logging.error('Incorrect verification token from Slack.')
    return {'username': slack_message['user_name'],
            'command': slack_message['command'].strip(),
            'arguments': slack_message['text'].strip(),
            'channel_name': slack_message['channel_name']}


def generate_slack_response(text, in_channel=True):
    """Consumes a string message to send to slack in a public format.

    If the message should be sent only to the user set in_channel=False
    """

    if in_channel:
        where_to_post = 'in_channel'
    else:
        where_to_post = 'ephemeral'
    response = {'response_type': where_to_post,
                'text': text,
                'attachments': list()}
    logging.debug('Slack Response: %s', str(response))
    return jsonify(response)


@APP.route('/', methods=['GET', 'POST'])
def icebreaker():
    """Post an icebreaker to slack, in response to user command."""

    logging.debug(request.form)
    slack_dict = parse_slack_message(request.form)
    in_channel = False
    if (slack_dict['command'] == COMMAND or
            slack_dict['command'] == '/' + COMMAND):
        if slack_dict['arguments'] == 'help':
            response = HELP
        elif not slack_dict['arguments']:
            response = get_icebreaker()
            logging.debug('Sending icebreaker: %s', response)
            in_channel = True
        else:
            logging.debug('Got unknown arguments: %s',
                          slack_dict['arguments'])
            response = ('Sorry, I don\'t know what %s means.\n %s' %
                        (slack_dict['arguments'], HELP))
    else:
        logging.debug('Got unknown command %s with arguments: %s',
                      slack_dict['command'],
                      slack_dict['arguments'])
        response = ('Sorry, I don\'t know what %s %s means.\n %s' %
                    (slack_dict['command'],
                     slack_dict['arguments'],
                     HELP))
    return generate_slack_response(response, in_channel=in_channel)


logging.basicConfig(level=logging.ERROR)
logging.debug('About to start running on port %d.', PORT)
ICEBREAKERS = load_icebreakers(DATA_FILE)
if ICEBREAKERS is None:
    sys.exit(1)


if __name__ == '__main__':
    APP.run(host=HOST, port=PORT, debug=True)
