# Bob the Ice Breaker Bot

[![pipeline status](https://gitlab.com/beautifulcanoe/peopleops/icebreaker-bot/badges/main/pipeline.svg)](https://gitlab.com/beautifulcanoe/peopleops/icebreaker-bot/-/commits/main)
[![MIT license](https://img.shields.io/badge/license-MIT-blue)](https://choosealicense.com/licenses/mit/)
[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

Bob the Ice Breaker Bot breaks the ice between colleagues on Slack channels, with a [slash command](https://api.slack.com/interactivity/slash-commands) such as `/icebreaker`.

## CI keys

This repository requires three CI variables for deploying Bob on [Heroku](https://dashboard.heroku.com/):

* `HEROKU_API_KEY` which is an API key for the user who owns the apps,
* `HEROKU_APP_STAGING` the name of a staging app; and
* `HEROKU_APP_PRODUCTION` the name of a production app.

The Heroku apps require three environment variables:

* `PORT` which is set automatically by Heroku,
* `SLASH_COMMAND` name of the slash command set in the Slack app, set to `icebreaker` by default.
* `VERIFICATION_TOKEN` Slack verification token for your Slack bot.

## Making changes to Bob the Ice Breaker Bot

Please see [CONTRIBUTING.md](/CONTRIBUTING.md) for advice on how to contribute to this repository.

## License

This work is licensed under the [MIT license](/LICENSE.md)
